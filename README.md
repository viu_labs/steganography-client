# Steganography Client #

A desktop client for steganography

### _Table of Contents_

- [How to Run on Mac](#How to Run on Mac)
- [How to Run on Ubuntu](#How to Run on Ubuntu)

### How to Run on Mac

1. Install `cmake` on your mac using `brew install cmake` 
2. Install `QT5` on your mac using `brew install qt@5`. Might need to explicitly set PATH="$(brew --prefix qt5)/bin:$PATH"
3. Build using cmake


### How to Run on Ubuntu (Steps tested with Ubuntu 19.10)

Install cmake (Mostly it will be there), verify using

```text
cmake --version
```

####Install QT5 -
[Refer this for more on "Install Qt 5 on Ubuntu"](https://wiki.qt.io/Install_Qt_5_on_Ubuntu)

Or simply,
```text
sudo apt-get install qt5-default
```
You can verify using,

```text
qmake --version
```

####Build using cmake, or use the build_script.sh

```text
./build_script.sh
```

####Run the `stegno_client` executable in the created /include folder

```text
./stego_client
```
