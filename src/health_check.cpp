#include <main_window.hpp>

void MainWindow::checkServerStatus() {
    bool status = false;

    while (run_srv_health_chk_) {
        QNetworkRequest request;

        {
            std::scoped_lock lock(connection_mtx_);
            request.setUrl(QUrl(health_url_.c_str()));
        }

        Q_EMIT signal_checkServerHealth(request);

        std::this_thread::sleep_for(std::chrono::seconds(5));
    }

    std::cout << "Stopping server health check" << std::endl;
}